﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindesheimAD2021AutoVerzekeringsPremie.Interfaces
{
    interface IVehicle
    {
        int PowerInKw();
        int ValueInEuros();
        int Age();
    }
}
