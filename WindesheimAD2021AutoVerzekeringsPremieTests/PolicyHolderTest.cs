﻿using System;
using Xunit;

using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using WindesheimAD2021AutoVerzekeringsPremie.Interfaces;

namespace WindesheimAD2021AutoVerzekeringsPremieTests
{
    public class PolicyHolderTest
    {
        //hieronder heb ik 2 testen geschreven die gebruik maken van een fact dit heb ik gedaan omdat er maar 2 verschillende soorten data nodig zijn
        //om alle functies te testen en ik verwachte ik ook gewoon dat de 2 testen een goed resultaat zouden geven en dat hebben ze ook gedaan
        [Fact]
        public void TestenOfAlsDePostalCodeBovenOfGelijkIsMet1000EnOnder3600IsHetDePremiumVermenigvuldigMet1Komma05()
        {
            //arrange
            double premium = 1000;

            int postalCode = 1100;

            double expected = 1050;

            //act

            double actual = PremiumCalculation.UpdatePremiumForPostalCode(premium, postalCode);

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestenOfAlsDePostalCodeOnder4500IsHetDePremiumVermenigvuldigMet1Komma02()
        {
            //arrange
            double premium = 1000;

            int postalCode = 3650;

            double expected = 1020;

            //act

            double actual = PremiumCalculation.UpdatePremiumForPostalCode(premium, postalCode);

            //assert
            Assert.Equal(expected, actual);
        }
    }
}
