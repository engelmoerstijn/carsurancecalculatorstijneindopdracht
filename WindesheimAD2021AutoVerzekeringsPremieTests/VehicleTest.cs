﻿using System;
using Xunit;

using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using WindesheimAD2021AutoVerzekeringsPremie.Interfaces;

namespace WindesheimAD2021AutoVerzekeringsPremieTests
{
    public class VehicleTest
    {
        //ik heb hieronder een test geschreven die gebruikt maakt van een fact dit heb ik gedaan omdat elke functie wordt getest ongeacht of er andere variable input is
        //toen ik de test ging schrijven wou ik gaan testen of de basispremie wel goed wordt berekent en ik verwachte dat als ik een nieuwe Vehicle zou aanmaken
        //en het in de functie zou doen dat die het goede antwoord zou geven maar het bleekt dat er een fout zat in de code er waren namelijk geen () neergezet waardoor /3
        //niet als laatste gebeurde dit heb ik gefixed door de () goed te schrijven

        [Fact]
        public void TestenOfDeBasisPremiumGoedWordtBerekentBijEenVehicle()
        {
            //Arrange
            Vehicle car = new Vehicle(510, 17000, 2012);

            double expectedPremium = 87.67;

            //Act
            double PremiumUitslag = Math.Round(PremiumCalculation.CalculateBasePremium(car),2);

            //Assert
            Assert.Equal(expectedPremium, PremiumUitslag);

        }
    }
}
