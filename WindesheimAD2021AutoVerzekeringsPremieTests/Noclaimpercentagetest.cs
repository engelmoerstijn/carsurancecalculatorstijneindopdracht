﻿using System;
using Xunit;

using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using WindesheimAD2021AutoVerzekeringsPremie.Interfaces;

namespace WindesheimAD2021AutoVerzekeringsPremieTests
{
    public class Noclaimpercentagetest
    {
        //ik heb hieronder een test geschreven die gebruik maakt van een theory dit heb ik gedaan omdat bij de claimpercentagetest het belangrijk is dat er verschillede 
        //soorten variable inputs verschillende functies moet testen en het makkelijker is om dat in 1 theory te doen dan met veel facts
        //toen ik deze test wou proberen verwachte ik dat het het wel zou doen maar toen ik het uitprobeerde was de uitkomst altijd 0 als het boven de 5 jaar is 
        //dit kwam omdat er int stond in plaats van double dat heb ik gefixed door double te schrijven en nu doet die het goed

        [Theory]
        [InlineData(1, 1, 1)]
        [InlineData(12, 5, 12)]
        [InlineData(1, 6, 0.95)]
        [InlineData(100, 7, 90)]
        [InlineData(10, 15, 5)]
        [InlineData(4, 50, 1.4)]
        public void TestenOfAlsJeVerschillendeJarenEnPremiumsNeemtJeWelDeVerwachteResultaatKrijgt(double premium, int years, double expectation)
        {
            double actual = PremiumCalculation.UpdatePremiumForNoClaimYears(premium, years);

            Assert.Equal(expectation, actual);
        }
    }
}
